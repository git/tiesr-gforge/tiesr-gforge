/*=======================================================================

 *
 * TIesrSD_User.h
 *
 * Header file to define TIesr Speaker Dependent speech recognition API
 * interface.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 This file defines the interface that implements a TIesr Speaker Dependent
 speech recognition API.  Application designers may use libraries that
 implement this API to voice enable their applications.  The API presents a
 simple and clean set of C functions that utilize the TIesrEngine API as the
 basis for the speech recognizer, and TIesrFA API as the frame audio data
 collection interface, without the application designer required to know the
 intricate details of these two APIs.

 The TIesrSD API consists of two subtasks, enrollment and recognition. The
 TIESRSD API can run only one of these subtasks at a time.  Enrollment and
 recognition have separate interface functions to start and end the subtask.

 Note that the TIesrSD API runs audio collection in a separate thread. It
 also runs the model creation step of the enrollment subtask, and the
 recognition step of the recognition subtask in a separate thread.  These two
 steps can take significant processing time, and so the application may have
 reason to terminate these steps prior to completion.

 ======================================================================*/


#ifndef _TIESRSD_USER_H
#define _TIESRSD_USER_H

#if defined (WIN32) || defined (WINCE)

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the TIESRSD_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// TIESRSD_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef TIESRSD_EXPORTS
#define TIESRSD_API __declspec(dllexport)
#else
#define TIESRSD_API __declspec(dllimport)
#endif

#else
#define TIESRSD_API 

#endif


/* Constants that are needed for TIesrSD */

/* 
   This is a requirement of the present TIesr Engine API. The app
   designer must ensure that exactly this number of utterances is recorded per
   enrollment.
*/
#define TIESRSD_NUM_ENR_UTT 2


/* 
   TIesrSD_Errors is an enumeration that defines the status values associated
   with operations provided by functions of the TIESRSD API.  A brief
   description of the errors is as follows:

   TIesrSDErrNone = No error, operation completed successfully.

   TIesrSDErrFail = The function failed to complete successfully.

   TIesrSDErrNoMemory = The function could not complete, out of memory.

   TIesrSDErrState = Attempt to do something out of sequence, for example,
   trying to start recognition while in the middle of enrollment.

   TIesrSDErrUtt = The application tried to record too many utterances.

   TIesrSDErrReco = The recognizer reported a failure during processing.

   TIesrSDErrAudio = The audio channel reported a collection failure.

   TIesrSDErrBoth = Both of recognizer and audio reported failures.

   TIesrSDErrSpeechEarly = Speech occurred prior to completion of adaptation
   to the present noise environment.

   TIesrSDErrThread = The recognizer thread could not start successfully.
*/
typedef enum TIesrSD_Errors
{
   TIesrSDErrNone,
   TIesrSDErrFail,
   TIesrSDErrNoMemory,
   TIesrSDErrState,
   TIesrSDErrUtt,
   TIesrSDErrReco,
   TIesrSDErrAudio,
   TIesrSDErrBoth,
   TIesrSDErrSpeechEarly,
   TIesrSDErrThread
} TIesrSD_Error_t;


/*----------------------------------------------------------------------------
 The TIesrSD_Object defines and contains all data that comprise an instance
 of the TIesrSD recognizer.  A pointer to this object is defined as
 TIesrSD_t.  The application designer will only interact with the recognizer
 through the pointer to the object, and will never operate directly on the
 contents of the TIesrSD_Object.
 ----------------------------------------------------------------------------*/
typedef struct TIesrSD_Object* TIesrSD_t;


/*------------------------------------------------------------------------
 TIesrSD provides results to the calling application by calling callback
 functions. The application designer will define these functions to handle
 asynchronous results produced by TIesrSD. These functions must execute
 quickly to maintain real time, and should normally just signal events that
 have occurred that should be processed within the main application thread in
 some kind of event loop processing.  A pointer argument is specified by the
 application to be used as an argument of the callback function.  TIesrSD
 also supplies an error value, indicating status information when the
 callback function is called.

 NOTE: The callback functions must not call any TIesrSD_* functions, since
 the callback functions are called outside of the main thread context.
 --------------------------------------------------------------------------*/

typedef void (*TIesrSD_Callback_t)( void* aAppArgument, TIesrSD_Error_t aError );


/*--------------------------------------------------------------------------
 The TIesrSD_Parameters object defines the parameters that may be set for
 the TIesrEngine, once the enrollment or recognition subtask is opened.
 During opening of the subtask, default values of these parameters are
 set. Once the subtask is opened, this object may be used to set values
 that tailor the enrollment or recognition for a specific application.
 These are advanced settings that can be used to improve performance of the
 recognizer and adjust between processing time and processing accuracy.
 Consult documentation for detailed description for setting each of these
 parameters.
 ----------------------------------------------------------------------------*/
typedef struct TIesrSD_Parameters
{
      /* TIesrSD parameters */
      short pruneFactor;
      short sadDelta;
      short sadMinDb;
      short sadBeginFrames;
      short sadEndFrames;
      short sadNoiseFloor;
      short lowVolume;
      short highVolume;

      /*audio channel parameters */
      int sampleRate;
      int circularFrames;
      int audioFrames;
      int audioPriority;
      int audioReadRate;
} TIesrSD_Params_t;


/* Use standard C function name locating conventions */
#ifdef __cplusplus
extern "C"
{
#endif

/*---------------------------------------------------------------------------
 TIesrSD_create creates an instance of the TIesrSD recognizer and initializes
 internal parameters necessary for operation of the recognizer.  It
 returns a pointer to the newly created TIesrSD_Object in the location
 specified by aPtrToTIesrSD.
 -------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_create( TIesrSD_t* aPtrToTIesrSD );


/*--------------------------------------------------------------------------
 TIesrSD_open opens an instance of the TIesrSD recognizer for a particular
 speaker-dependent application.  The input arguments define the audio device
 to be used for audio data collection, the grammar directory holding the
 universal phonetic models used during enrollment, and the model directory.
 All models enrolled will be placed in the model directory, and recognition
 of an input utterance will be compared to all models in the directory.  The
 arguments also include the two callback functions used during audio
 recording to inform the application when the user may speak and when the
 recording has finished, and a void pointer to user application data that
 will be passed in the callback function.  The speakCallback function may or
 may not be called, since the user may stop recording before it is time to
 speak.  After a successful start of recording, the doneCallback function
 is guaranteed to be called either due to end of utterance, or due to some
 other condition that stops recording.
 ----------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_open( TIesrSD_t aTIesrSD,
   const char* aAudioDevice,
   const char* aGrammarDir,
   const char* aModelDir,
   TIesrSD_Callback_t speakCallback,
   TIesrSD_Callback_t doneCallback,
   void* const callbackData );


/*--------------------------------------------------------------------------
 TIesrSD_enroll_open opens the enrollment subtask of the TIesrSD recognizer
 instance.  This function can be called once the TIesrSD instance has been
 opened.  It specifies the memory sizes that will be used during enrollment.
 When enrollment is complete, there must be a call to TIesrSD_enroll_close to
 complete the enrollment subtask.
 ---------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_open( TIesrSD_t aTIesrSD,
   const unsigned int aMemorySize,
   const unsigned int aMaxFrames );


/*------------------------------------------------------------------------
  Once the enrollment subtask is opened, then multiple enrollments of items
  can take place.  Each enrollment consists of initialization, recording
  utterances, and creation of the enrollment model.  At any time, the
  procedure can be restarted by initializing again.  The TIesrSD_enroll_init
  function performs initialization. 
-----------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_init( TIesrSD_t aTIesrSD );


/*-------------------------------------------------------------------------
 Record an enrollment utterance.  Callback functions will be called within
 the recording thread to indicate when it is ok for the user to speak, and
 when the recording has completed.  See TIesrSD_open.
 ---------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_record( TIesrSD_t aTIesrSD );


/*------------------------------------------------------------------------
 The TIesrSD_enroll_stop function must be called once after each successful
 call to TIesrSD_enroll_record, and before any other subsequent TIesrSD*
 call. It may be called prior to the completion of recording in order to
 force the stopping of recording an enrollment utterance.  Since recording
 is asynchronous, the recording may complete normally prior to this function
 completing.  The user application is responsible for handling all callback
 functions correctly.  After this function is called and returns, it is
 guaranteed that the recording thread has terminated.  If the user
 application calls this function to stop recording prior to successful
 completion of a recording, then no further enrollment can be done without
 first re-initializing, and starting the recording process over again.  See
 TIesrSD_enroll_init above.
 ----------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_stop( TIesrSD_t aTIesrSD );


/*---------------------------------------------------------------------------
 TIesrSD_enroll_model creates a model from the utterances that were
 just recorded.  The second argument specifies the file name of the
 file to hold the enrolled model.  The directory path where the file
 name is located is specified in TIesrSD_open.  Note that prior to
 calling this function to enroll the model, the user application
 must have recorded exactly TIESRSD_NUM_ENR_UTT utterances.

 Note: At some time this should be made an asynchronous process that could
 be stopped prior to completion, since it takes a significant amount of
 time to create an enrollment model.
 -------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_model( TIesrSD_t aTIesrSD, const char* aFileName );


/*-----------------------------------------------------------------------
 TIesrSD_enroll_score returns the enrollment score after performing model
 enrollment.  This score provides a means to confirm that the enrollment
 model matches the two enrollment utterances.
 --------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_score( TIesrSD_t aTIesrSD, short* aScore );


/*-------------------------------------------------------------------------
 TIesrSD_enroll_close closes the enrollment subtask. This allows TIesrSD to
 subsequently open the recognition subtask, or to close.
 --------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_close( TIesrSD_t aTIesrSD );



/*-------------------------------------------------------------------------
 TIesrSD_reco_open opens the recognition subtask of the TIesrSD recognizer
 instance.  This function can be called once the TIesrSD instance has been
 opened.  It specifies the memory sizes that will be used during recognition.
 When recognition is complete, there must be a call to TIesrSD_reco_close to
 close the recognition subtask.
 ---------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_open( TIesrSD_t aTIesrSD,
   const unsigned int aMemorySize,
   const unsigned int aMaxFrames );


/*---------------------------------------------------------------------
 Once the recognition subtask is opened, then multiple recognition of
 items can take place.  Each recognition consists of initialization,
 recording an utterance, and scoring the utterance with all models in
 the model directory.  At any time, the procedure can be restarted by
 initializing again.  The TIesrSD_reco_init function performs
 initialization.
 ---------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_init( TIesrSD_t aTIesrSD );


/*--------------------------------------------------------------------------
 Record a recognition utterance.  Callback functions will be called within
 the recording thread to indicate when it is ok for the user to speak, and
 when the recording has completed.  See TIesrSD_open.
 ---------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_record( TIesrSD_t aTIesrSD );


/*-------------------------------------------------------------------------
 The TIesrSD_reco_stop function must be called once after each successful
 call to TIesrSD_reco_record, and before any other subsequent TIesrSD*
 call. It may be called prior to the completion of recording in order to
 force the stopping of recording the recognition utterance.  Since recording
 is asynchronous, the recording may complete normally prior to this function
 completing.  The user application is responsible for handling all callback
 functions correctly.  After this function is called and returns, it is
 guaranteed that the recording thread has terminated.  If the user
 application calls this function to stop recording prior to successful
 completion of a recording, then no further recognition can be done without
 first re-initializing, and starting the recording process over again.  See
 TIesrSD_reco_init above.
 ----------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_stop( TIesrSD_t aTIesrSD );


/*--------------------------------------------------------------------------
 Score the recorded utterance versus all of the models in the model directory
 specified in TIesrSD_Open.  Return the filename and score of the best
 scoring file.  This can only be called after the recognition task is opened,
 initialized and an utterance is recorded.  The best file is returned by copying
 the string, so the user app must ensure it points to an array that can hold
 the file name.
 --------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_score( TIesrSD_t aTIesrSD,
   char* aBestFile,
   long* aBestScore );

/*------------------------------------------------------------------------
 Get the confidence in the recognition result.  This compares the score
 of the best result to the background model score.
 ------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_confidence( TIesrSD_t aTIesrSD,
   short* confScore );


/*-------------------------------------------------------------------------
 Close the recognition subtask.  This will release all resources created when
 the subtask was opened.  This allows the TIesrSD instance to either open
 another recognition subtask, the enrollment subtask, or close the instance.
 ----------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_close( TIesrSD_t aTIesrSD );


/*---------------------------------------------------------------------------
 Once the enrollment or recognition subtask is opened, the user application
 can set several parameters that affect the operation of the enrollment or
 recognition subtask, as defined by the TIesrSD_Parameters object.  These
 parameters all are initialized with default values when the subtask is
 opened, but may be changed after opening.  Parameters can only be changed
 in the subtask open state.  Below are the functions for manipulating the
 parameters. Once the subtask is opened, the TIesrSD_Parameters object may
 be used to set values that tailor the enrollment or recognition subtask
 for a specific application.  These are advanced settings that can be used
 to improve performance and also adjust between processing time and
 processing accuracy.  Consult documentation for a detailed description for
 setting each of these parameters.
 ----------------------------------------------------------------------------*/
       
/* Fill a TIesrSD_Parameters object with the values presently in use */
TIESRSD_API
TIesrSD_Error_t TIesrSD_getparams( TIesrSD_t aTIesrSD,
   TIesrSD_Params_t* aParams );


/* Set parameters TIesrSD will use to values in the TIesrSD_Parameters object. */
TIESRSD_API
TIesrSD_Error_t TIesrSD_setparams( TIesrSD_t aTIesrSD,
   const TIesrSD_Params_t* aParams );


/*-------------------------------------------------------------------------
 After recording an utterance, this function can be used to determine the
 number of frames of data of speech detected by the recording.  This can
 be used by the user application for a variety of purposes.  For example,
 this can be used to determine that an utterance to enroll is too short.
 -------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_getframecount( TIesrSD_t aTIesrSD,
   short* aFrameCount );


/*--------------------------------------------------------------------
 After recording an utterance, this function can be used to determine
 whether the speech recorded was spoken at a low or high volume, as
 determined by the threshold values set via TIesrSD_setparams.
 ------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_getvolume( TIesrSD_t aTIesrSD, short* aVolumeLevel );
				   

/*---------------------------------------------------------------------
 Get detailed status information for the result of the last operation.
 This status consists of two unsigned integers. One specifies
 detailed engine status, and the other detailed status of audio
 collection.  Normally, this function is not called, but may be useful
 during debugging of an application to determine the cause of failures.
 The values are not protected by any synchronization object and are set
 as errors occur.
 -----------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_status( TIesrSD_t aTIesrSD,
   int* aEngineStatus,
   int* aAudioStatus );

/*---------------------------------------------------------------------------
 Close down the TIesrSD recognizer.  This will deallocate any resources that
 were created when the recognizer was opened.  The recognizer can then be
 opened again, for example, with a new model directory for a different
 speaker dependent task.
 --------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_close( TIesrSD_t aTIesrSD );


/*---------------------------------------------------------------------------
 Destruct the instance of the TIesrSD recognizer.  This completely removes the
 TIesrSD object that was created with TIesrSD_create, and frees all resources
 that were allocated internally.  This must be called as the last part of
 using a recognizer instance to ensure that all allocated resources have
 been deallocated and no leakage occurs.
 ---------------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_destroy( TIesrSD_t aTIesrSD );


/* Wrap up use of standard C function name locating conventions */
#ifdef __cplusplus
}
#endif


#endif  /* _TIESRSD_USER_H */
