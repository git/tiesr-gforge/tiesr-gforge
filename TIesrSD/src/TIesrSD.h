/*===============================================================

 *
 * TIesrSD.h
 *
 * This file defines the functions and data structures that are internal to
 * the TIesrSD API that the user does not need access to.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

  =================================================================*/

#ifndef _TIESRSD_H
#define _TIESRSD_H

/* System C library functions needed */
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>

/* The TIesr Engine API for SD enrollment and recognition with live audio */
#define USE_AUDIO
#include <tiesr_engine_api_sdenro.h>
#include <tiesr_engine_api_sdreco.h>


/* The TIesr frame audio capture API */
#include <TIesrFA_User.h>

/* The TIesrSD User API */
#include "TIesrSD_User.h"

/* Multi-threading support APIs */
#if defined (LINUX)
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#elif defined (WIN32) || defined (WINCE)
#endif


/* Debug logging flag.  This results in output debug logging to files
   for each thread. This line should normally be commented out in order
   to build a version of TIESRSD that does not do debug logging. Debug
   logging is not an option that the user sees within the API. */

/* #define TIESRSD_DEBUG_LOG */

#ifdef TIESRSD_DEBUG_LOG

#if defined (WINCE)
#define LOGFILE "\\Application Data\\TIesr\\SDlog.txt"
#define THREADFILE "\\Application Data\\TIesr\\SDthread.txt"
#else
/* Define location of log files for other platforms. */
#endif

#endif

#if defined (WIN32) || defined (WINCE)
#define FILESEPCHR "\\"
#else
#define FILESEPCHR "/"
#endif



/* Constants used within the API */

#define TRUE 1
#define FALSE 0


/* Audio default constants */

/* Sample rate in Hz */
#define SAMPLE_RATE 8000

/* Blocking flag for audio */
#define BLOCKING TRUE

#if defined (LINUX)

/* Number of frames of data in circular buffer to maintain real time */
#define CIRCULAR_FRAMES 100
/* Number of frames of data read per call to the audio device */
#define AUDIO_FRAMES 5
/* Audio priority of the frame audio thread */
#define  AUDIO_PRIORITY 20
/* Time between audio device read attempts (if needed) in microseconds */
#define AUDIO_READ_RATE 10000

#elif defined (WIN32) || defined (WINCE)

/* Number of frames of data in circular buffer to maintain real time */
#define CIRCULAR_FRAMES 500
/* Number of audio buffers holding data read from the audio device */
#define AUDIO_FRAMES 10
/* Audio priority of the frame audio thread */
#define  AUDIO_PRIORITY  THREAD_PRIORITY_ABOVE_NORMAL
/* For Windows TIesrFA this is the number of frames of audio data that
each audio buffer returned from the audio device driver will hold */
#define AUDIO_READ_RATE 2

#endif



/* The number of frames that must be used to collect background prior to 
   the user speaking during recording */
#define TIESRSD_START_FRAMES 10


/* Define state for doing wordspotting during confidence scoring */
#define WORDSPOT TRUE


/* Definition of error types for local functions */
typedef enum TIesrSDL_Errors
{
  TIesrSDLErrNone,
  TIesrSDLErrFail,
  TIesrSDLErrAudio,
  TIesrSDLErrRecord,
  TIesrSDLErrThread,
  TIesrSDLErrNoMemory,
  TIesrSDLErrNoModels
} TIesrSDL_Error_t;


/*
  Definition of the allowable states of the TIesrSD recognizer.
*/
typedef enum TIesrSDL_State
{
   TIesrSDStateClosed,
   TIesrSDStateOpen,
   TIesrSDStateEnroOpen,
   TIesrSDStateEnroRecord,
   TIesrSDStateRecoOpen,
   TIesrSDStateRecoRecord
}TIesrSDL_State_t;


/* 
   The TIesrSD_Object is a structure that holds all information and data
   necessary to encapsulate an instance of the TIesrSD recognizer.
*/

typedef struct TIesrSD_Object
{
      
   /* The TIesr Engine API SD enrollment engine */
   TIesrEngineSDENROType enroengine;
   
   /* The TIesr Engine API SD recognition engine */
   TIesrEngineSDRECOType recoengine;

   short* srchmemory;
   short* framedata;
   char* grammardir;
   char* modeldir;      
   char** modellist;
   int nummodels;
   TIesrEngineStatusType engstatus;

   /* The audio channel and its parameters */
   TIesrFA_t audiodevice;
   unsigned int samplerate;
   unsigned int circularframes;
   unsigned int audioframes;
   int audiopriority;
   int audioreadrate;
   char* devicename;
   TIesrFA_Error_t audiostatus;

      
   /* TIesrSD API recognizer parameters */
   int state; /* State of the recognizer */
   

#if defined (LINUX)
      /* Recognizer thread while recording */
      pthread_t threadid; 
      
      /* Recognizer mutex protecting shared variables */
      pthread_mutex_t recordmutex; 
      
      /* Semaphore to detect recording started */ 
      sem_t startsemaphore; 

      /* Flag indicating recording thread started ok */
      int startok;

      /* Flag to indicate a request to stop recording thread */
      int stoprecord;
      
#elif defined (WIN32) || defined (WINCE)
      /* Recognizer thread while recording */
      HANDLE threadid; 
      
      /* Recognizer mutex protecting shared variables */
      HANDLE recordmutex; 
      
      /* Event to detect recording started */ 
      HANDLE startevent; 

      /* Flag indicating recognizer thread started ok */
      int startok;

      /* Flag to indicate a request to stop recognition thread */
      int stoprecord;

#endif   
   

  
   /* Callback functions to generate recording events to main thread */
   TIesrSD_Callback_t speakcb;
   TIesrSD_Callback_t donecb;
   void* cbdata;

   /*
    Initialization and completion flags.  These act as substates
    of the enrollment or recognition subtasks.
    */
   int initialized;
   int complete;

   /* Number of utterances recorded. */
   int uttcount;
   
   
   /* log output file pointer, allowing capture of
    debug logging data */
   #if defined(TIESRSD_DEBUG_LOG)
   FILE* logfp;
   FILE* threadfp;
   #endif


} TIesrSD_Object_t;


/* Local function prototypes - not visible to the user API */
static TIesrSDL_Error_t TIesrSDL_freeopen( TIesrSD_t aTIesrSD );
static TIesrSDL_Error_t TIesrSDL_start_record( TIesrSD_t aTIesrSD );
static TIesrSDL_Error_t TIesrSDL_stop_record( TIesrSD_t aTIesrSD );
static TIesrSDL_Error_t TIesrSDL_initsync( TIesrSD_t aTIesrSD );
static TIesrSDL_Error_t TIesrSDL_resetsync( TIesrSD_t aTIesrSD );
static TIesrSDL_Error_t TIesrSDL_reco_list( TIesrSD_t aTIesrSD );
static TIesrSDL_Error_t TIesrSDL_free_list( TIesrSD_t aTIesrSD );

#if defined (LINUX)
static void* TIesrSDL_thread( void* aArg );
#elif defined (WIN32) || defined (WINCE)
static DWORD TIesrSDL_thread( LPVOID aArg );
#endif

#endif  /* _TIESRSD_H */
