#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# LinuxDebugGnu configuration
CND_PLATFORM_LinuxDebugGnu=GNU_current-Linux-x86
CND_ARTIFACT_DIR_LinuxDebugGnu=../../Dist/LinuxDebugGnu/bin
CND_ARTIFACT_NAME_LinuxDebugGnu=testtiesrsd
CND_ARTIFACT_PATH_LinuxDebugGnu=../../Dist/LinuxDebugGnu/bin/testtiesrsd
CND_PACKAGE_DIR_LinuxDebugGnu=dist/LinuxDebugGnu/GNU_current-Linux-x86/package
CND_PACKAGE_NAME_LinuxDebugGnu=testtiesrsd.tar
CND_PACKAGE_PATH_LinuxDebugGnu=dist/LinuxDebugGnu/GNU_current-Linux-x86/package/testtiesrsd.tar
# LinuxReleaseGnu configuration
CND_PLATFORM_LinuxReleaseGnu=GNU_current-Linux-x86
CND_ARTIFACT_DIR_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/bin
CND_ARTIFACT_NAME_LinuxReleaseGnu=testtiesrsd
CND_ARTIFACT_PATH_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/bin/testtiesrsd
CND_PACKAGE_DIR_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU_current-Linux-x86/package
CND_PACKAGE_NAME_LinuxReleaseGnu=testtiesrsd.tar
CND_PACKAGE_PATH_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU_current-Linux-x86/package/testtiesrsd.tar
# ArmLinuxDebugGnueabi configuration
CND_PLATFORM_ArmLinuxDebugGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/bin
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabi=testtiesrsd
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/bin/testtiesrsd
CND_PACKAGE_DIR_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabi=testtiesrsd.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package/testtiesrsd.tar
# ArmLinuxReleaseGnueabi configuration
CND_PLATFORM_ArmLinuxReleaseGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/bin
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabi=testtiesrsd
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/bin/testtiesrsd
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabi=testtiesrsd.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package/testtiesrsd.tar
