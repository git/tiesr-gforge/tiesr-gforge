/*===============================================================
 
 *
 * loadmfcc.cpp
 *
 * This source defines functions that load and manipulate mfcc data for
 * the TIesr SD API.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 =====================================================================*/

#include "tiesr_config.h"

#include "tiesrcommonmacros.h"
#include "winlen.h"
#include "gmhmm_type.h"
#include "mfcc_f_user.h"
#include "loadmfcc_user.h"

/*--------------------------------*/
/* Local defines */
#define MU_SCALE(i, scale_p2) (1<<scale_p2[i])


/*--------------------------------*/
/*
** MFCC the whole file
*/
/* REC should not be defined for live mode  */
#ifdef REC
extern short *mu_scale_p2;

void pt_fix(char *mesg, short x[], ushort q, ushort N)
{
  ushort i;
  printf("%s ", mesg);
  for (i=0; i< N; i++) printf("%8.3f ", x[i]/(float)(1<<q)* (1<<mu_scale_p2[i]));
  
  printf("\n");
}



void _pt_fix(char *mesg, short x[], ushort q, ushort N)
{
  ushort i;
  printf("%s ", mesg);
  for (i=0; i< N; i++) printf("%7.4f ", x[i]/(float)(1<<q));
  printf("\n");
}

void __pt_fix(char *mesg, short x[], ushort q, ushort N, FILE *pf)
{
  ushort i;
  float tmp;
  
  for (i=0; i< N; i++) {
    tmp = x[i]/(float)(1<<q);
    fwrite(&tmp,sizeof(float),1,pf);
  }
}
#endif

//extern void prt_freq(char *mesg, short x[], unsigned short q, unsigned short N);
/*
** no SAD, unpacked, no delta mfcc; estimate noise from silence
*/


/*--------------------------------*/
TIesrEngineStatusType load_mfcc_1(gmhmm_type *gvv, short mfcc[], ushort max_frms, char fname[])
{
  gmhmm_type *gv = (gmhmm_type *) gvv;
  long  cum_log_mel_energy[ N_FILTER26 ]; 
  short log_mel_energy[ N_FILTER26 ];
  FILE *fp;
  ushort i;  
  short n_read;
  ushort mfcc_idx = 0;
  short power_spectrum[ WINDOW_LEN ];
  NormType var_norm;
  int max_size =  max_frms * gv->n_mfcc;
  //  FILE *pf;
  //  char str[200];
  
  //  sprintf(str,"%s.lm.20",fname);
  //  printf("new file log-mel= %s\n", str);
  //  pf = fopen(str,"wb");
  
  
  gv->frm_cnt = 0;

  fp = fopen(fname, "rb");      /* binary speech */
  if (!fp) {
    PRT_ERR(fprintf(stderr,"open failed: %s\n", fname));
    exit(0);
  }
  n_read = fread(gv->sample_signal, sizeof(short), WINDOW_LEN, fp);
  for (i = 0; i < gv->n_filter; i++) cum_log_mel_energy[i] = 0;
  while ( n_read >= FRAME_LEN ) {

    if (( mfcc_idx + gv->n_mfcc) >= max_size ) {
      PRT_ERR(fprintf(stderr, "Maximum utterance duration (%d frames) reached\n", max_frms));
      return eTIesrEngineFeatureFrameOut;
    }
    /* compute mfcc for this window */
    mfcc_a_window(gv->sample_signal, mfcc + mfcc_idx, log_mel_energy, 
		  gv->n_mfcc, gv->n_filter, gv->mel_filter, gv->cosxfm, power_spectrum, gv->muScaleP2, &var_norm, &(gv->last_sig), NULL);
    
    //    __pt_fix("log-mel",  log_mel_energy, 9, gv->n_filter,pf);

    if ( gv->frm_cnt <  PMCFRM ) 
      for (i = 0; i < gv->n_filter; i++) cum_log_mel_energy[i] += log_mel_energy[i];  /* Q 9 */
    
    /* overlap */
    for (i = 0; i < WINDOW_LEN - FRAME_LEN; i++)
      gv->sample_signal[ i ] = gv->sample_signal[ i + FRAME_LEN ];

    /* read next frame */
    n_read = fread(gv->sample_signal + WINDOW_LEN - FRAME_LEN, sizeof(short), FRAME_LEN, fp);
    gv->frm_cnt++;
    
    mfcc_idx += gv->n_mfcc;
  }
  fclose(fp);
  //  fclose(pf);
  for (i=0; i< gv->n_filter; i++) {
    /* use the first PMCFRM frames to estimate noise profile */
    gv->log_N[ i ] = (short) (cum_log_mel_energy[i] >> PMCFRM_P2 ); 
  }

  //  prt_freq("noise-freq", gv->log_N, 9, gv->n_filter);  
  return eTIesrEngineSuccess; // frm_cnt_frontend; /* total frame count for the file */
}


/*--------------------------------*/
/*  This function does not appear to be used now 
void prt_fix(char *mesg, short x[], const short scale_p2[], unsigned short N, FILE *pf)
{
  unsigned short i;
  float tmp;
  //  printf("%s ", mesg);
  //  for (i=0; i< N; i++) printf("%7.4f ", x[i] * MU_SCALE(i,scale_p2)/ (float)(1<<11) );
  for (i=0; i< N; i++) {
    tmp = x[i] * MU_SCALE(i,scale_p2)/ (float)(1<<11) ;
    fwrite(&tmp,sizeof(float),1,pf);
  }
}
*/


/*--------------------------------*/
TIesrEngineStatusType Load_mfcc_(gmhmm_type *gvv, char fname[])
{
  gmhmm_type *gv = (gmhmm_type *) gvv;
  //  return load_mfcc_1(gvv, (short *)gv->mem_feature, gv->max_frame_nbr, fname);
  TIesrEngineStatusType status;
  //  int f;
  //  char newfile[ 200 ];
  //  FILE *pf;
  
  status = load_mfcc_1(gvv, (short *)gv->mem_feature, gv->max_frame_nbr, fname);

  /*
  sprintf(newfile, "%s.%s.%d",fname, "CS",gv->n_mfcc);
  printf("File = %s\n", newfile);
  pf = fopen(newfile,"wb");

  if (!pf) {
    fprintf(stderr,"can't open %s\n", newfile);
    exit(0);
  }


  for (f = 0; f < gv->frm_cnt; f++ ){
    prt_fix("mfcc", (short *)gv->mem_feature + f * gv->n_mfcc, gv->muScaleP2,  gv->n_mfcc, pf);
  }
  fclose(pf);
  */
  return status;
}
