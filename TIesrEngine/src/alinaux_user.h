/*===============================================================

 *
 * alinaux_user.h
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 
 ==================================================================*/

#ifndef _ALINAUX_USER_H
#define _ALINAUX_USER_H

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"
#include "gmhmm_type.h"

short test_convergence(short new_like, short last_like, ushort iter);

void reverse_list(ushort low, ushort high, ushort start, gmhmm_type *gv);

#endif /* _ALINAUX_USER_H */
