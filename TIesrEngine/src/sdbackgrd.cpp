/*===============================================================

 *
 * sdbackgrd.cpp
 *
 * This source defines the function TIesrsdbgd.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/

// Binary split algorithm for background model generation
// - cluster mean vectors of all existing models
// - use average variance over all models

#include <stdio.h>

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"

#include "gmhmm_type.h"
#include "pack_user.h"
#include "dist_user.h"

#include "sdvar_header.h"
#include "sdhead.h"
#include "sdreco.h"
#include "sd_reco_type.h"
#include "sdreco_user.h"

/*--------------------------------*/
/* Local defines and allocations */

//#define PRT_BGD

#define NBR_ITR 3

// extern char word_list[ MAX_WORD ][ MAX_STR ]; 

#define MIN_VEC 80


/*--------------------------------*/
/*
void prt_mu(char *mesg, short x[], ushort q, ushort N)
{
  ushort i;
  printf("%s ", mesg);
  for (i=0; i< N; i++) printf("%7.4f ", x[i]/(float)(1<<q)* (1<<mu_scale_p2[i]));
  printf("\n");
}
*/


/*--------------------------------*/
/*
** return the max cum likelihood over the model
*/

static long  read_prms_cluster(char *dir, char *name, short cluster[], short var[], short nbr_class, long acc[],
			ushort count[], gmhmm_type *gv)
{
  char fname[MAX_STR];
  FILE *pf_data;
  short tmp[SD_N_MFCC], vec[SDDIM], cls, cls_idx;
  ushort j, nbr_hmm_1, nbr, i, total_emission_states = 0; /* also the number of mean vectors */
  long  total_scr, scr, diff, sum_scr = 0;
  short diff_s;
  short *mu, *invvar, gconst, *feat;

  sprintf(fname,"%s/%s%s", dir, name, SDEXT);
  pf_data = fopen(fname,"rb");
  fread(&nbr_hmm_1, sizeof(ushort),1,pf_data); /* read the size */

#ifdef BIT8VAR 
  gconst = GCONSTSP; 
#else
  gconst = gauss_det_const( var, 2, gv->muScaleP2, SDDIM );
#endif
  /* count nbr of mean vectors */
  for (i=0; i<nbr_hmm_1; i++) {
    fread(&nbr, sizeof(ushort),1,pf_data); /* read 1 nbr of stt */
    total_emission_states += nbr - 1;
  }
  /* read mean vectors */

  for (i=0; i< total_emission_states; i++) {
    read_one_vector(pf_data, tmp); // in vec is 8 shorts 
    vector_unpacking((ushort *)tmp, vec, gv->scale_mu,SD_N_MFCC);
    cls_idx = 0;
    total_scr = BAD_SCR;
    for (cls=0; cls<nbr_class; cls++) {
      invvar = var;
      feat = vec;
#ifdef BIT8MEAN
      mu = cluster + cls * SDDIM;
      GAUSSIAN_DIST_MU16(scr, feat,j, SDDIM, diff, diff_s, gconst, mu, invvar, SD_N_MFCC, gv->scale_var);
#else
      mu = cluster + cls * SDDIM;
      GAUSSIAN_DIST(gv, scr, feat,j, SDDIM, diff, diff_s, gconst, mu, invvar);
#endif
      if (total_scr < scr) { 
	total_scr = scr;
	cls_idx = cls;
      }
    }
    sum_scr += total_scr;
    for (j=0; j < SDDIM; j++) acc[cls_idx * SDDIM + j] += vec[j];
    count[cls_idx]++;
    // printf("max likelohood = %f, class = %d\n", total_scr/64., cls_idx);
  }
  fclose(pf_data);
  return sum_scr;
}


/*--------------------------------*/
/*
** cum of all mean vectors of a name
*/
static ushort read_prms_mu(char *dir, char *name, long cum[], gmhmm_type *gv)
{
  char fname[MAX_STR];
  FILE *pf_data;
  ushort j, nbr_hmm_1, nbr, i, total_emission_states = 0; /* also the number of mean vectors */
  short vec[SDDIM];
#ifdef BIT8MEAN
  short tmp[SD_N_MFCC];
  //  extern short *scale_mu;
#endif

  sprintf(fname,"%s/%s%s", dir, name, SDEXT);
  pf_data = fopen(fname,"rb");
  fread(&nbr_hmm_1, sizeof(ushort),1,pf_data); /* read the size */

  /* count nbr of mean vectors */
  for (i=0; i<nbr_hmm_1; i++) {
    fread(&nbr, sizeof(ushort),1,pf_data); /* read 1 nbr of stt */
    total_emission_states += nbr - 1;
  }
  /* read mean vectors */
  for (i=0; i< total_emission_states; i++) {
#ifdef BIT8MEAN
    read_one_vector(pf_data, tmp);
    vector_unpacking((ushort *)tmp, vec, gv->scale_mu,SD_N_MFCC);
#else
    read_one_vector(pf_data, vec);
#endif
    for (j=0; j<SDDIM; j++) cum[j] += vec[j];
  }
  fclose(pf_data);
  return total_emission_states;
}


/*--------------------------------*/
/*
** get the average of variance vector
*/

//void read_prms_var(char *name, long cum_var[])
//{
//  char fname[MAX_STR];
//  FILE *pf_data;
//  short var_org[SDDIM];
//  ushort j;
//  long offset = SDDIM * sizeof(short);
// 
//  sprintf(fname,"%s%s", name, SDEXT);
//  pf_data = fopen(fname,"rb");
//  if (!pf_data) {
//    fprintf(stderr,"open failed: %s\n", fname);
//    exit(0);
//  }
//  /* offset to veriance vectors: */
//  fseek (pf_data, -offset, 2);
//
//  /* read the variance: */
//  fread(var_org,sizeof(short),SDDIM, pf_data);
//  for (j=0; j < SDDIM; j++) cum_var[j] += var_org[j];
//  fclose(pf_data);
//}


/*
** return actuall number of classes, which may be less than the required_nbr_cls
** clusters not packed.
*/


/*--------------------------------*/
/*
** generate background model 
*/
ushort TIesrsdbgd(ushort nbr_words, char *directory, char *name_list[], gmhmm_type *gv, sd_reco_type *sdv)
{
  short *pclusters = sdv->clusters;
  long *mem = sdv->mem_blc;
  ushort nbr_class = 1;
  ushort req_nbr_cls = MAX_NBR_CLS;
  long *accumulator = mem;
  long  *sum = accumulator + SDDIM * MAX_NBR_CLS, syst_scr; 
  short itr, i, d;
  ushort total_vec, sum_vec;
#ifdef PRT_BGD  
  char *out_dir = "C:/temp";
  //  char *out_dir = "/home/ygong/tmp";
#endif
  
  short *avg_var =
#ifdef BIT8VAR
    (short *)sdv->unique_pr;
#else  
    pclusters + SDDIM * MAX_NBR_CLS; 
#endif
  ushort count[SDDIM];
  ushort max_nbr_cls;
#ifdef PRT_BGD  
      FILE *po;
      char fname[MAX_STR];
  
   if (out_dir) {
    sprintf(fname,"%s/cluster_output.txt", out_dir);
    po = fopen(fname,"w");
    if (!po) fprintf(stderr,"can't open %s\n", fname);
  }
  else po = stdout;
  // background model calculation 
  fprintf(po,"%d models\n", nbr_words); 
#endif

  //  for (i=0; i<SDDIM; i++) sum[i] = 0;
  /* average variance: */
  //  for (i = 0; i < nbr_words; i++) read_prms_var(name_list[i], sum);
  //  for (i=0; i< SDDIM; i++) avg_var[i] = (short)(sum[i] /  nbr_words);
  /* copy variance to this memory (alternative: direct use of unique_pr[])): */

#ifndef BIT8VAR
  for (i=0; i< SDDIM; i++) 
     //avg_var[i] = (short *)unique_pr[i]; /* packed array */
     avg_var[i] = unique_pr[i]; /* packed array */
#endif

  //   prt_variance(avg_var);

  for (i=0; i<SDDIM; i++) sum[i] = 0;

  /* center of gravity: */
  total_vec = 0;
  for (i = 0; i < nbr_words; i++) total_vec += read_prms_mu(directory,name_list[i], sum, gv);
  for (i=0; i< SDDIM; i++) pclusters[i] = (short)(sum[i] /  total_vec);
  //  prt_mu("GLB MU", pclusters, 11, SDDIM);
#ifdef PRT_BGD  
  fprintf(po,"%d vectors\n",  total_vec);
#endif
  max_nbr_cls = (total_vec >= MIN_VEC)? req_nbr_cls: 8;
  while (nbr_class < max_nbr_cls) {
    /* split */
    for (i = 0; i < nbr_class; i++) {
      for (d=0; d < SDDIM; d++) {
	short delta = pclusters[d + i * SDDIM] >> 9; 
	pclusters[d + (i+ nbr_class) * SDDIM ] = pclusters[d + i * SDDIM] + delta;  /* destination */
	pclusters[d+ i * SDDIM] -= delta;                                          /* source      */
      }
    }
    nbr_class <<=1 ; /* we ow have 2N classes */
    /* moving clusters: */
    for (itr = 0; itr < NBR_ITR; itr++) {
      for (i=0; i<nbr_class; i++) {
	for (d=0; d < SDDIM; d++) accumulator[i * SDDIM + d] = 0;
	count[ i ] = 0;
      }
      /* iteration for clustering: */
      syst_scr = 0;
      for (i = 0; i < nbr_words; i++) 
	syst_scr += read_prms_cluster(directory, name_list[i], pclusters, avg_var, nbr_class, accumulator, count, gv);
      /* end loop */
      sum_vec = 0;
      for (i=0; i<nbr_class; i++) {
	//Change this code so that it does not print and exit.
	if (count[i] == 0) { 
	  PRT_ERR(fprintf(stderr,"too few training vectors\n"));
	  PRT_ERR(exit(0)); 
	  return 0;
	}
	for (d=0; d < SDDIM; d++) pclusters[i * SDDIM + d] = accumulator[i * SDDIM + d] / count[i];
#ifdef PRT_BGD  
	fprintf(po,"%4d ", count[i]);
#endif
	sum_vec += count[i];
      }
#ifdef PRT_BGD  
          fprintf(po,"\nsystem likelihood = %f\n", syst_scr/64./total_vec);
#endif
      //      if (sum_vec != total_vec) fprintf(stderr,"background model: sum_vec != total_vec\n");
    }
  }
#ifdef PRT_BGD  
    if (out_dir) fclose(po);
#endif
  return nbr_class;
}
