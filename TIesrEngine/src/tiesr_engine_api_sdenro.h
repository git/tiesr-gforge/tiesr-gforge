/*================================================================

 *
 * tiesr_engine_api_sdenro.h
 *
 * This header defines the interface of the TIesr SD enrollment.
 * It was modified to make the TIesr code more modular.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ==================================================================*/

#ifndef _TIESR_ENGINE_API_SDENRO_H
#define _TIESR_ENGINE_API_SDENRO_H



#include "tiesr_config.h"
#include "status.h"

/* This is only included here, since the user must know how many
   samples per frame to supply to the recognizer. */

#include "winlen.h"


/* This is only included here, since the user must know how many
   utterances are needed to complete an enrollment. */
#include "sdparams.h"

/* Typedefs to specify what each API function pointer type is, and enable
 compiler checking. */

typedef struct sd_enro_type* sd_enro_type_p;

#ifndef SD_GMHMM_TYPE_P
#define SD_GMHMM_TYPE_P
typedef struct gmhmm_type* gmhmm_type_p;
typedef struct gmhmm_type const * const_gmhmm_type_p;
#endif

/*--------------------------------*/
/* 
   The TIesrEngineSDENROType structure defines an instance of the
    TIesr SD enrollment API.  The user must instantiate and delete an
    instance by opening or closing the instance.
*/

/*
** scope limitation: API function visible only within variable
*/
typedef struct {
/*
** load initial model
*/
   
  /* TIesrSDEnroInit */  
  TIesrEngineStatusType (*Open)(char *, sd_enro_type_p, int, unsigned short);

/*
** free memory for initial model
*/
  /* TIesrSDEnroRelease */ 
  TIesrEngineStatusType (*Close)( sd_enro_type_p );

/*
** to be done before enrolling every item or before loading a reco item
*/
  /* TIesrSDMemInit */
  void (*Init)( sd_enro_type_p );

/*
** score (log likelihood) in Q6:
*/

  /* TIesrSDEnrGetSco */
  short (*GetScore)( sd_enro_type_p );


/*
** get common (enro/reco)  part of SD
*/
  /* TIesrSDEnroData */ 
  gmhmm_type_p (*GetRecordData)( sd_enro_type_p );

/*
** enrol one item 
*/
  /* TIesrSDEnro */
  TIesrEngineStatusType (*Enroll)(char *path, sd_enro_type_p );

/*
** load test utterance from file, with no utterance detection.
*/

#ifndef USE_AUDIO
 /* TIesrSDEnrLoad */ 
 TIesrEngineStatusType (*Load)(char fname[], sd_enro_type_p );
#endif

  /*
  ** with utterance detection
  */
  /* TIesrSDRecordOpen */
  void (*RecordOpen)( gmhmm_type_p );

  /* TIesrSDRecord */ 
  TIesrEngineStatusType (*Record)( gmhmm_type_p, short sig[], short );
  
  /* TIesrSDRecordClose */
  TIesrEngineStatusType (*RecordClose)( gmhmm_type_p );

  /* GetSDFrameCount */
  short (*GetFrameCount)( const_gmhmm_type_p );

  /* TIesrSDSpeechEnded */
  short (*SpeechEnded)( gmhmm_type_p );
  
  short (*SpeechDetected)( gmhmm_type_p );

  short (*GetVolumeStatus)( const_gmhmm_type_p ); 

#ifdef USE_AUDIO
  void (*SetSADPrams)( gmhmm_type_p, short, short, short, short, short);
  void (*SetVolumeRange)( gmhmm_type_p, unsigned short, unsigned short);

  void (*GetSADPrams)( const_gmhmm_type_p, short*, short*, short*, short*, short*);
  void (*GetVolumeRange)( gmhmm_type_p, unsigned short*, unsigned short*);
#endif

} TIesrEngineSDENROType;

//#define USE_DLL 

#ifdef USE_DLL
#ifdef  TIESR_ENGINE_SDENRO_API_EXPORTS
#define TIESR_ENGINE_SDENRO_API __declspec(dllexport)
#else
#define TIESR_ENGINE_SDENRO_API __declspec(dllimport)
#endif
#else /* not USE_DLL */
#define TIESR_ENGINE_SDENRO_API
#endif /* USE_DLL */


#ifdef __cplusplus
extern "C"
{
#endif

TIESR_ENGINE_SDENRO_API void TIesrEngineSDENROOpen (TIesrEngineSDENROType *);

TIESR_ENGINE_SDENRO_API void TIesrEngineSDENROClose(TIesrEngineSDENROType *);

#ifdef __cplusplus
}
#endif

#endif /* _TIESR_ENGINE_API_SDENRO_H */
