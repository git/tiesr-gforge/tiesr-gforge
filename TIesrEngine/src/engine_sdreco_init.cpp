/*=============================================================

 *
 * engine_sdreco_init.cpp
 *
 * This source defines the API interface to the user of the TIesr
 * SD recognizer for recognition.  This API has been changed to
 * improve modularity.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ===================================================================*/

#include "tiesr_config.h"

#include "gmhmm_type_common_user.h"
#include "uttdet_user.h"
#include "volume_user.h"

#include "tiesr_engine_api_sdreco.h"
#include "gmhmm_sd_api.h"



TIESR_ENGINE_SDRECO_API 
void TIesrEngineSDRECOOpen (TIesrEngineSDRECOType *sdreco)
{
  sdreco->Open           = TIesrSDRecOpen;
  sdreco->Close          = TIesrSDRecClose;
  sdreco->LoadBackground = TIesrSDBgd;
  sdreco->Init           = TIesrSDRecInit;
  /* sdreco->SetPrune       = SetTIesrSDPrune; */
  sdreco->SetPrune       = SetTIesrPrune;
  /* sdreco->GetPrune       = GetTIesrSDPrune; */
  sdreco->GetPrune       = GetTIesrPrune;
  sdreco->GetConfidence  = TIesrSDConf;
#ifndef USE_AUDIO
  sdreco->Load           = TIesrSDRecLoad;
#endif
  sdreco->Score          = TIesrSDSco;

  sdreco->GetRecordData = TIesrSDRecoData;

  /*
  ** life mode 
  */

  sdreco->RecordOpen = TIesrSDRecordOpen_1;
  sdreco->Record      = TIesrSDRecord;
  sdreco->RecordClose = TIesrSDRecordClose;

  /* sdreco->GetFrameCount = GetSDFrameCount; */
  sdreco->GetFrameCount = GetFrameCount;
  /* sdreco->SpeechEnded = TIesrSDSpeechEnded; */
  sdreco->SpeechEnded = SpeechEnded;
  /* sdreco->SpeechDetected = TIesrSDSpeechDetected; */
  sdreco->SpeechDetected = SpeechDetected;
  
  /* sdreco->GetVolumeStatus = GetSDVolumeStatus; */
  sdreco->GetVolumeStatus = GetVolumeStatus;

#ifdef USE_AUDIO
  sdreco->SetSADPrams = SetTIesrSAD;
  sdreco->SetVolumeRange = SetTIesrVolRange; 

  sdreco->GetSADPrams = GetTIesrSAD;
  sdreco->GetVolumeRange = GetTIesrVolRange; 
#endif

  return ;
} 

TIESR_ENGINE_SDRECO_API void TIesrEngineSDRECOClose(TIesrEngineSDRECOType *)
{
  return;
}

