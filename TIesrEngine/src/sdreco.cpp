/*==================================================================

 *
 * sdreco.cpp
 *
 * This source defines the processing that is performed to implement
 * TIesr SD recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/

/*
** File based GMHMM speaker dependent recognition using enrolled models
*/
#include <stdio.h>

#include "tiesr_config.h"
#include "status.h"
#include "tiesrcommonmacros.h"

#include "gmhmm_type.h"
#include "search_user.h"
#include "pack_user.h"
#include "pmc_f_user.h"
#include "mfcc_f_user.h"
#include "search_user.h"

#include "sd_reco_type.h"
#include "sdhead.h"
#include "sdreco.h"
#include "sdvqpack_user.h"
#include "sdauxl_user.h"
#include "sdreco_user.h"


/*--------------------------------*/
void read_one_vector(FILE *pf_data, short vec[])
{
#ifdef BIT8MEAN
/*
** return packed vector
*/
#ifdef VQMODEL /* models are VQ-ed, need expand: */
  { 
    ushort coded[ VECSIZE ];
    fread(coded, sizeof(short), VECSIZE, pf_data);
    vq_to_pack(coded, vec);
  }
#else
  fread(vec, sizeof(short), SD_N_MFCC, pf_data);
#endif
  
#else
  int d;
  ushort tmp_sh;
  for (d = 0; d < SD_N_MFCC; d++) {
    fread(&tmp_sh, sizeof(short),1, pf_data);
    DECODE_STDY(tmp_sh, vec[d], vec[d + SD_N_MFCC]);
  }
#endif
}


/*--------------------------------*/
/*
** initialize silence HMM and variance vector
** may consider merging with new_reco-gtm 
*/
static void init_silence_varc(ushort hmm_silence, const short precision_vec[], gmhmm_type *gv, sd_reco_type *sdv)
{
  ushort j;
  short *p_mix;
  HmmType *hmm;

  short nbr_dim = 
#ifdef BIT8VAR
  SD_N_MFCC;
#else
  SDDIM;
#endif

  for (j=0; j<nbr_dim; j++) {
    /* assign the variance for speech */
    gv->base_var[j] = precision_vec[j];
    /* inititialize silence variance (the seocnd vector): */
    (gv->base_var + nbr_dim)[j] = sdv->var_1[j];
  }
  /* assign gconst */
  gv->base_gconst[0] = GCONSTSP; // gauss_det_const(base_var,2); /* ~ 42.0 */
  gv->base_gconst[1] = GCONSTSL; /* 30.390862, Q6 for silence model */

  /* repositioning silence variance to 1 (was positioned to 0 in new_reco_gtm) */ 
  hmm = GET_HMM(gv->base_hmms, hmm_silence, 0);
  FOR_EMS_STATES(j, hmm,gv->base_tran) {
    p_mix = GET_MIXING_COMPONENT(GET_BJ(gv,hmm,j));
    INDEX_FOR_VARC(p_mix) = 1; /* silence variance is in this memory */	
  }
}


/*--------------------------------*/
/*
** copy mu values to bease_mu.
*/
static void assign_mu(short clusters[], short nbr_class, ushort stt, gmhmm_type *gv)
{
  ushort c;
  unsigned short j;
  /* for other HMMs: assign mean vectors for each Gaussian: */

#ifdef BIT8MEAN
  for (c=0; c < nbr_class; c++) 
    vector_packing(clusters + c * SDDIM, (ushort *) gv->base_mu + stt + c * SD_N_MFCC, gv->scale_mu, SD_N_MFCC);
#else  
  for (j=0; j < SDDIM * nbr_class; j++) gv->base_mu[j] = clusters[j];
#endif
}


/*--------------------------------*/
/*
** 1. init state information
** 2. call new_reco_gtm to construct gtm structure
** 3. copy HMM mean values
** 4. copy background mu (no background HMM, but silence with large mixture)
** last emission proba is silence 
*/
static ushort get_init_gtm(short clusters[], short var[], short nbr_class, char *dir, char *name, ushort *mem_count, 
		    ushort max_memory_blc, TIesrEngineStatusType *the_status, gmhmm_type *gv, sd_reco_type *sdv)
{
  char fname[MAX_STR];
  ushort    *state_nbr; /* not need for decoding */
  
  FILE *pf_data;
  ushort j, nbr_hmm_1, i, current_mean = 0; /* also the number of mean vectors */
  
#ifdef BIT8MEAN
  short nbr_dim = SD_N_MFCC;
#else
  short nbr_dim = SDDIM;
#endif


  sprintf(fname,"%s/%s%s", dir, name, SDEXT);
  pf_data = fopen(fname,"rb");
  if (!pf_data) {
    fprintf(stderr,"open failed: %s\n", fname);
    exit(0);
  }
  fread(&nbr_hmm_1, sizeof(ushort),1,pf_data); /* read the size, not including silence */
  state_nbr = sdv->mem_base; 
  *mem_count += nbr_hmm_1 /* name hmm */ + 1 /* silence */; 
  if (!cap_in_check("number of satates", *mem_count,max_memory_blc)){
    *the_status = eTIesrEngineStateMemoryOut;
    return 0;
  }

  fread(state_nbr, sizeof(ushort),nbr_hmm_1,pf_data); /* read the array */
  
  state_nbr[nbr_hmm_1] = 2; /* for silence, which is located at the END of hmm (and mu) list */
  *the_status = new_spot_reco_gtm(nbr_hmm_1 + 1, state_nbr, sdv->mem_base, mem_count, max_memory_blc, nbr_class, gv);  
  if (*the_status != eTIesrEngineSuccess) return 0;
  
  init_silence_varc(nbr_hmm_1, sdv->unique_pr, gv, sdv); /* var of background mode is set to the same as speech */
  
  /* for other HMMs: current_mean is the index of the current (static part of) mean vector */
  for (i=0; i<nbr_hmm_1; i++) {
    for (j=0; j < state_nbr[i] - 1; j++) {
      read_one_vector(pf_data,gv->base_mu + current_mean);
      current_mean += nbr_dim;
    }
  }
  fclose(pf_data);

  assign_mu(clusters, nbr_class, current_mean, gv);
  *the_status = eTIesrEngineSuccess;
  return nbr_hmm_1 + 1; /* number of HMMs for this name */
}


/*--------------------------------*/
/*
** construct gtm for background model and then assign mean vectors
** (a modification of get_init_gtm())
** The background model is a silence model with mixture of gaussians.
*/
static ushort get_bgd_gtm(short clusters[], short var[], short nbr_class, ushort *mem_count, 
		   ushort max_memory_blc, TIesrEngineStatusType *the_status, gmhmm_type *gv, sd_reco_type *sdv)
{
  ushort nbr_hmm_1; /* also the number of mean vectors */
  ushort *state_nbr; 
  
  nbr_hmm_1  = 0; /* bgd model */
  state_nbr = sdv->mem_base; 
  *mem_count += nbr_hmm_1 + 1;

  if (!cap_in_check("number of satates", *mem_count,max_memory_blc)) {
    *the_status = eTIesrEngineStateMemoryOut;
    return 0;
  }

  state_nbr[nbr_hmm_1] = 2; /* for silence, which is located at the END of hmm (and mu) list */

  *the_status = new_spot_reco_gtm(nbr_hmm_1 + 1, state_nbr, sdv->mem_base, mem_count, max_memory_blc, nbr_class, gv);  

  if (*the_status != eTIesrEngineSuccess) return 0;
  
  //  init_silence_varc(nbr_hmm_1, var);
  init_silence_varc(nbr_hmm_1, sdv->unique_pr, gv, sdv);

  assign_mu(clusters, nbr_class, 0,gv);
  *the_status = eTIesrEngineSuccess;
  return nbr_hmm_1 + 1; /* number of HMMs for this name */
}


/*--------------------------------*/
/*
** make recognition model for backgroud (a modification of load_1_net_and_gtm())
*/
TIesrEngineStatusType make_bgd_net_and_gtm(short clusters[],short nbr_class, ushort memory_size, gmhmm_type *gv, sd_reco_type *sdv)
{
  ushort nbr_hmm;
  /* points to the last available memory index, initialized for every name: */
  ushort mem_count = 0;
  TIesrEngineStatusType the_status;
  short *var = 
#ifdef BIT8VAR
    (short *)sdv->unique_pr;
#else  
    clusters + MAX_NBR_CLS * SDDIM; /* variance vector */
#endif
  nbr_hmm = get_bgd_gtm(clusters, var, nbr_class,&mem_count, memory_size, &the_status, gv, sdv);  
  if (the_status != eTIesrEngineSuccess) return the_status;
  else return make_net(gv, nbr_hmm, &mem_count, (short *)sdv->mem_base, memory_size);
}


/*--------------------------------*/
/*
**  load a name model and its HMMs into memory: 
*/
TIesrEngineStatusType load_1_net_and_gtm(char *dir, char *network_name, ushort memory_size, 
				    short clusters[], short nbr_class, gmhmm_type *gv, sd_reco_type *sdv)
{
  ushort nbr_hmm;
  /* points to the last available memory index, initialized for every name: */
  ushort mem_count = 0; 
 TIesrEngineStatusType the_status;
  short *var = 
#ifdef BIT8VAR
    (short *)sdv->unique_pr;
#else  
    clusters + MAX_NBR_CLS * SDDIM; /* variance vector */
#endif
  nbr_hmm = get_init_gtm(clusters, var, nbr_class, dir, network_name,&mem_count, memory_size, &the_status, gv, sdv);  
  if (the_status != eTIesrEngineSuccess) return the_status;
  else return make_net(gv, nbr_hmm, &mem_count, (short *)sdv->mem_base, memory_size);
}


#ifdef BIT8MEAN
/*--------------------------------*/
/*
** PMC all HMMs' mean vector, silence is not compensated but copied
*/
void pmc_all_hmms(gmhmm_type *gv) // short *noise_log_mel_energy, short *chn)
{
  short   i, j;
  short   *orig, tmp[ SDDIM ];

  extern void prt_vec(char *str, short *v, unsigned short n);
  for (i = 0; i < gv->n_mu - 1; i++) { /* last one is silence */
    orig = gv->base_mu + i * SD_N_MFCC;
    pmc(orig, gv->log_N, orig, gv->log_H, 0, SD_N_MFCC, gv->n_filter, gv->muScaleP2, gv->scale_mu, gv->cosxfm, NULL, TRUE );
   }
  /* copy noise estimate to silence mean */
  vector_unpacking((ushort *) gv->base_mu + (gv->n_mu-1) * SD_N_MFCC, tmp, gv->scale_mu, SD_N_MFCC);
  inverse_cos_transform(gv->log_N, tmp, gv->muScaleP2, SD_N_MFCC, gv->n_filter, gv->cosxfm);
  for (j = 0; j < SD_N_MFCC; j++) tmp[j + SD_N_MFCC] = 0; /* dynamic */
  vector_packing(tmp, (ushort *) gv->base_mu + (gv->n_mu-1) * SD_N_MFCC, gv->scale_mu, SD_N_MFCC);
}

#else



//void
//pmc_all_hmms(short *noise_log_mel_energy, short *chn)

void pmc_all_hmms(gmhmm_type *gv) // short *noise_log_mel_energy, short *chn)
{
  short   i, j;
  short   *orig;


  /* correct all model means except silence using pmc only */
  for (i = 0; i < gv->n_mu - 1; i++) { /* last one is silence */
     orig = gv->base_mu + i * SDDIM;
     // pmc(orig, noise_log_mel_energy, orig, chn, 0, SD_N_MFCC);
     pmc(orig, gv->log_N, orig, gv->log_H, 0, SD_N_MFCC, 
	gv->n_filter, gv->muScaleP2, gv->scale_mu, gv->cosxfm, NULL, FALSE );
   }

  /* copy noise estimate to silence mean */
  // inverse_cos_transform(noise_log_mel_energy, base_mu + (n_mu-1) * SDDIM, mu_scale_p2, SD_N_MFCC);
  inverse_cos_transform(gv->log_N, gv->base_mu + (gv->n_mu-1) * SDDIM, gv->muScaleP2, SD_N_MFCC, 
			gv->n_filter, gv->cosxfm );
 
  for (j = 0; j < SD_N_MFCC; j++) gv->base_mu[ (gv->n_mu-1) * SDDIM + j + SD_N_MFCC] = 0; /* dynamic */
}

#endif


/*--------------------------------*/
/*
** recognize one file, adapted from main.c of GMHMM. no utterance detection.
*/
short rec_process_a_file(gmhmm_type *gv)
{
  short idx = 0, type;
  short i, j;
  ushort mfcc_idx;
  short reg_mfcc[ SD_N_MFCC ];
  short mfcc_feature[ SDDIM ];   
  short the_frm_cnt = 0;
  short idx2, idx3;                  /* to prime buffer, HTK compatible */
  short frm_cnt_frontend;                            /* used in main.c   */
  short status;
  /* compute regression and search */

  status = eTIesrEngineSuccess;
 
  for (mfcc_idx = 0, frm_cnt_frontend = 0;  status == eTIesrEngineSuccess && frm_cnt_frontend < gv->frm_cnt;
       mfcc_idx += SD_N_MFCC, frm_cnt_frontend++) {

    /* put into circular buffer, for regression computation */
    idx = circ_idx( frm_cnt_frontend );
    for (i = 0; i < SD_N_MFCC; i++)
      gv->mfcc_buf[ idx ][i] = gv->mem_feature[ mfcc_idx + i];
   
    if ( frm_cnt_frontend == 0 ) {                /* prime the buffer, HTK compatible */
      for (i = 1; i < MFCC_BUF_SZ; i++) {
		for (j = 0; j < SD_N_MFCC; j++) {
		  gv->mfcc_buf[i][j] = gv->mfcc_buf[0][j];
		}
      }
    }      

    if ( frm_cnt_frontend >= REG_SPAN ) {

      type = frm_cnt_frontend < (REG_SPAN * 2) ? 0 : 1;    /* beg or regular ? */
      idx = circ_idx( idx - REG_SPAN );

      //      compute_regression( idx, 1, reg_mfcc, SD_N_MFCC );     /* buffer primed, type 1 */
      compute_regression( idx, 1, reg_mfcc, gv->n_mfcc, gv->muScaleP2,  gv->mfcc_buf);


    for (i = 0; i < SD_N_MFCC; i++) {
      mfcc_feature[i] = gv->mfcc_buf[idx][i];
      mfcc_feature[i + SD_N_MFCC] = reg_mfcc[i];
    }
      /* search entry */
    status = search_a_frame(mfcc_feature, 1, the_frm_cnt, gv);
    //  status = search_a_frame( 1, frm_cnt );
    the_frm_cnt++;
    }
  }   

  /* end type regression frames */

  for (j = 0;  the_frm_cnt < gv->frm_cnt && status == eTIesrEngineSuccess && j < REG_SPAN; j++) {

    idx = circ_idx( idx + 1 );

    idx2 = circ_idx( idx + 1 );
    idx3 = circ_idx( idx + 2 );
    for (i = 0; i < SD_N_MFCC; i++)                  /* prime the buffer */
      gv->mfcc_buf[idx3][i] = gv->mfcc_buf[idx2][i];

      /* buffer primed, type 1 */
    compute_regression( idx, 1, reg_mfcc, gv->n_mfcc, gv->muScaleP2,  gv->mfcc_buf);
    
    for (i = 0; i < SD_N_MFCC; i++) {
      mfcc_feature[i] = gv->mfcc_buf[idx][i];
      mfcc_feature[i + SD_N_MFCC] = reg_mfcc[i];
    }
    /* search entry */
    
    //  status = search_a_frame( 1, frm_cnt);
    status = search_a_frame(mfcc_feature, 1, the_frm_cnt, gv);

    the_frm_cnt++;
  }

  /* empty frame */

  if ( status == eTIesrEngineSuccess)  status = search_a_frame(NULL, 0, gv->frm_cnt - 1, gv );
  return status;
  //  return gv->best_sym_scr /* /total_frm */ ; /* division unnecessary */
}
