/*=================================================================

 *
 * sd_reco_type.h
 *
 * This header exposes the sd_reco_type structure for use in
 * TIesr SD recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 =====================================================================*/
/*
** structure for SD recognition. Algo+data API. 
*/


#ifndef _SD_RECO_TYPE_H
#define _SD_RECO_TYPE_H

#include "tiesr_config.h"

#include "tiesrcommonmacros.h"
#include "gmhmm_type.h"

#include "sdhead.h"

/*--------------------------------*/
/* Defines */

/*
** name dialing  memory specification:
*/

#define MAX_NBR_CLS 16 /* required number of classes */


/* (flash) memory (calculated before recognition)
** clustering results, additional SDDIM  for variance
*/
#define MUSIZE (SDDIM * MAX_NBR_CLS)


/*
** clustering results, the last is variance: SDDIM/2 enough for BIT8VAR
*/

#ifdef BIT8VAR
#define CLUSTR_MEM_SIZE  (MUSIZE)
#else
#define CLUSTR_MEM_SIZE  (SDDIM * (MAX_NBR_CLS + 1))
#endif


/*--------------------------------*/
typedef struct {
  ushort best_length; /* speech duration */ // best_stt, best_stp;  
  long best_sco;
} MaxType;


/*--------------------------------*/
typedef struct sd_reco_type
{

  short *org_scale_mu;    /* backup the variable for enro */
  short *org_scale_var;

  const short *_scale_var;
  const short *var_1;
  const short *unique_pr;

  MaxType memo;
  ushort *mem_base;       /* location for (grammar + hmm) */
  ushort nbr_class;       /* number of clusters of the background model */
  //  ushort total_mem_size;  /* total memory (in short) given by ther application */
  
/*
** needed all the time during recognition. should be stored in flash.
*/
  short clusters[CLUSTR_MEM_SIZE]; /* storing mean vectors of background models, unpacked  */
  //  ushort total_mem_base[ MAX_MEM ]; /* MEMORY FOR RECOGNITION: */
/*
** this memory could be part of mem_base: only needed during clustering:
*/
  long *mem_blc ; /* shared w/ search space 
		     [MUSIZE + SDDIM] accumulation of mu,  the last SDDIM is for sum[] */
  
  gmhmm_type *gv;         /* recognizer structure */
} sd_reco_type;




#endif /* _SD_RECO_TYPE_H */
