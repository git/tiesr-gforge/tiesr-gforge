/*===============================================================

 *
 * sdhead.h
 *
 * This header defines constants used within the TIesrSD enrollment and
 * recognition apis.  These constants are not exposed outside of the
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ======================================================================*/


#ifndef _SDHEAD_H
#define _SDHEAD_H

#include "tiesr_config.h"


/*
**  Commenting this out will disable VQ modeling:
*/
#define VQMODEL


/*
#define SELF_ 1
#define NEXT_ 8
#define SKIP_ 1

1.000000 0.000000 0.000000 
1.000000 8.000000 1.000000 0.000000 
0.000000 1.000000 8.000000 1.000000 
0.000000 0.000000 1.000000 8.000000 
----
0.000000 -10000000000.000000 -10000000000.000000 
0 -32768 -32768 
-2.302585 -0.223144 -2.302585 -10000000000.000000 
-147 -14 -147 -32768 
-10000000000.000000 -2.302585 -0.223144 -2.302585 
-32768 -147 -14 -147 
-10000000000.000000 -10000000000.000000 -2.197225 -0.117783 
-32768 -32768 -140 -7 
*/

/* for regular state: */
#define TR_SELF -147 /* Q6 */
#define TR_NEXT -14
#define TR_SKIP  TR_SELF

/* for last state only:   */
#define TR_SELF_LST  -140
#define TR_NEXT_LST  -7 

#ifdef REC
/* for silence only: */
#define TR_SELF_SIL -11
#define TR_NEXT_SIL -114 
#endif


/* max hmm emit state number in generated models  */

#define MAX_NBR_EM_STATES 8  
#define SD_N_MFCC 8 

/* Maximum number of segments in an enrollment */
#define SD_MAX_NBR_SEGS 40


/* name dialing uses 8-dim mfcc + 8 dynamic */
#define SDDIM (SD_N_MFCC*2) 


/* For the API, we don't want any extension.  The application designer
   has complete control of the file name where the data will be stored. 
*/
#define SDEXT ""                         
//#define SDEXT ".mod"

/* output ASCII form of network */
//#define OUTPUT_NET

#define MIN_RECORD_FRM 50



// These are used in places (such as in sdreco.cpp) even if BIT8VAR not set
//#ifdef BIT8VAR
/*
**  gconst for speech variance, calculated from speech variance using
*/
#define   GCONSTSP  2642 /*  gauss_det_const(base_var,2); (~ 42.0) */
#define   GCONSTSL  1945  /* 30.390862, Q6 for silence model */
//#endif


#ifdef VQMODEL
#define MIXN 16 /* number of classes for VQ */
/* This should be included as an api if VQMODEL is set, not in this header.
extern void vq_to_pack(unsigned short coded[], short packed[]);
extern void pack_to_vq(short packed[], unsigned short coded[]);
*/
#define VECSIZE 4
#endif

#endif /* _SDHEAD_H */
