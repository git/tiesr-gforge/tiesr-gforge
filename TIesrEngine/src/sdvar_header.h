/*==================================================================

 *
 * sdvar_header
 *
 * This header contains constants that are used to define fixed variance
 * values used for SD recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ===================================================================*/

#ifndef _SDVAR_HEADER_H
#define _SDVAR_HEADER_H

#include "tiesr_config.h"

/*
** to be included once and only once  in reco or enro.
*/



/* NOTE: These two 16-bit arrays were originally in sdvar.cpp */

/*
** initial silence variance: scaled Q9 of 1/variance
*/

const short var_1[] = { 5503, 703, 2689, 1543, 1075, 1578,  554,  494, 
			3162, 1075, 5669,  982, 1519, 3555, 1661, 7071, };

/*
** unique vector for precision vector: (average of all precision vectors ov. spk, names)
*/
const short unique_pr[] = {6413, 1498, 1910, 731, 691, 968, 249, 313, 
			   1194, 1072, 1495, 548, 488, 697, 716, 914,};





const short Z_scale_var[] = {
  2,   4,   3,   4,   4,   4,   5,   6,   3,   4,   2,   5,   4,   3,   4,   2, 
};

/* 
** packed version of the above (packing procedure in model_sd.cpp)
*/

const short Z_var_1[] = {22115, 11331, 21593, 24699, 17247, 25455, 17768, 31854, };
const short Z_unique_pr [] = {25637, 24131, 15383, 11845, 11039, 15638,  7981, 19982, };


#endif
